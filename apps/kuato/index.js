function sum(a, b) {
  return a + b;
}

function notTestedFunction(a, b) {
  return a + b;
}

module.exports = sum;
