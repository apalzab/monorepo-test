const min = require("./index");

test("adds 1 + 2 to equal 3", () => {
  expect(min(1, 2)).toBe(-1);
});
